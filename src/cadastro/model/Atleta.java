package cadastro.model;

import java.awt.Component;

public class Atleta {
	
	private String nome;
	private int idade;
	private Endereco endereco;
	private PilotoF1 categoria;
	
	public String toString(){  
        return nome+"  "+categoria.getEquipe()+"\n";  
    }  
	
	public void setNome(String nome){
		this.nome = nome;
	}
	public PilotoF1 getCategoria() {
		return categoria;
	}
	public void setCategoria(PilotoF1 categoria) {
		this.categoria = categoria;
	}
	public String getNome() {
		return nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
}
