package cadastro.Model;

public class PilotoF1 extends Atleta{

	PilotoF1(String nome) {
		super(nome);
		// TODO Auto-generated constructor stub
	}
	private String equipe;
	private int numeroPiloto;
	private int numeroTitulos;
	
	public String getEquipe() {
		return equipe;
	}
	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}
	public int getNumeroPiloto() {
		return numeroPiloto;
	}
	public void setNumeroPiloto(int numeroPiloto) {
		this.numeroPiloto = numeroPiloto;
	}
	public int getNumeroTitulos() {
		return numeroTitulos;
	}
	public void setNumeroTitulos(int numeroTitulos) {
		this.numeroTitulos = numeroTitulos;
	}
	

}
