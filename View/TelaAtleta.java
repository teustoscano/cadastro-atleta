package cadastro.View;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import cadastro.Controller.*;
import cadastro.Model.*;

public class TelaAtleta {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Menus menu = new Menus();
		Atleta atleta = new Atleta(null);
		ControlaAtleta controles = new ControlaAtleta();
		//Endereco endereco = new Endereco();
		Scanner lerTeclado = new Scanner(System.in);
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		int opcaoMenu = -1;
		
		
		
		while(opcaoMenu != 0){
			menu.menuInicial();
			opcaoMenu = lerTeclado.nextInt();
		switch(opcaoMenu){
			case 1:
				System.out.println("\f");
				System.out.println("\n\n\t Adicionar atleta");
				System.out.println("Digite o nome: ");
				atleta = new Atleta(in.readLine());
				System.out.println("Digite a idade: ");
				atleta.setIdade(lerTeclado.nextInt());
				System.out.println("Digite Seu pais: ");
				Endereco endereco = new Endereco();
				endereco.setPais(lerTeclado.next());
				System.out.println("Digite sua cidade: ");
				endereco.setCidade(in.readLine());
				
				atleta.setEndereco(endereco);
				controles.adicionar(atleta);
				break;
			case 2:
				System.out.println("\f");
				System.out.println("\t Remover atleta");
				String nomeAtleta;
				System.out.println("Digite o nome do atleta a ser removido: ");
				nomeAtleta = in.readLine();
				atleta = controles.pesquisarAtleta(nomeAtleta);
				controles.remover(atleta);
				break;
			case 3:
				System.out.println("\f");
				System.out.println("\t Pesquisa de atleta");
				System.out.println("Digite o nome do atleta: ");
				String nomePiloto = in.readLine();
				atleta = controles.pesquisarAtleta(nomePiloto);
				if(atleta == null){
					System.out.println("Atleta não encontrado!");
				}else{
				System.out.println(atleta.getNome());
				System.out.println(atleta.getIdade());
				System.out.println(atleta.getEndereco().getPais());
				}
				break;
			case 4:
				System.out.println("\t Lista de Atletas");
				controles.atletasNaLista();
				break;
			case 5:	
				System.exit(0);
			default:
				System.out.println("Opção invalida!");
				break;
		}
		}
	}

}
